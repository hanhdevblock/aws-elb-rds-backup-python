from __future__ import print_function
import os
import sys
from time import strftime, sleep
import boto3
from botocore.exceptions import ClientError
#VERSION_LABEL = strftime("%Y%m%d%H%M%S")
#BUCKET_KEY = os.getenv('APPLICATION_NAME') + '/' + VERSION_LABEL + \
#    '-bitbucket_builds.zip'

def rds_backup_manual():
    try:
        client = boto3.client('rds')
    except ClientError as err:
        print("Failed to create boto3 client.\n" + str(err))
        return False
    try:
        print("RDS Snapshot Backup:" + RDS_SNAPSHOT_IDENTIFY)
        client.create_db_snapshot(
            DBSnapshotIdentifier=os.getenv('RDS_SNAPSHOT_IDENTIFY'),
            DBInstanceIdentifier=os.getenv('RDS_INSTANCE_IDENTIFY')
        )
    except ClientError as err:
        print("Failed to created snapshot on RDS.\n" + str(err))
        return False
    return True

def rds_cluster_backup():
    try:
        client = boto3.client('rds')
    except ClientError as err:
        print("Failed to create boto3 client.\n" + str(err))
        return False
    try:
        client.create_db_cluster_snapshot(
            DBClusterSnapshotIdentifier=os.getenv('RDS_SNAPSHOT_IDENTIFY'),
            DBClusterIdentifier=os.getenv('RDS_INSTANCE_IDENTIFY')
        )
    except ClientError as err:
        print("Failed to created Cluster snapshot on RDS.\n" + str(err))
        return False
    return True

def main():
    isCluster=os.getenv('CLUSTER')
    if isCluster:
        rds_cluster_backup()
    else:
        rds_backup_manual()
    sys.exit(1)

if __name__ == "__main__":
    main()
