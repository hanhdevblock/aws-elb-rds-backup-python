FROM python:3.5.1
MAINTAINER hanhnh@devblock.net
COPY . /
RUN apt-get update && apt-get install -y zip && pip install boto3==1.3.0
CMD [ "python","beanstalk_deploy.py" ]
